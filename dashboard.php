<html>
<style>
	form.example input[type=text] {
	  padding: 10px;
	  font-size: 17px;
	  border: 1px solid grey;
	  float: left;
	  width: 80%;
	  background: lightgrey;
	}

	form.example input[type=button] {
	  align: center;
	  width: 20%;
	  padding: 10px;
	  background: #2196F3;
	  color: white;
	  font-size: 17px;
	  border: 1px solid grey;
	  border-left: none;
	  cursor: pointer;
	}

	.pagination a {
		color: black;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color.red;
        }

	.pagination a.active {
    	background-color: dodgerblue;
        color: white;
	}

	.pagination a:hover:not(.active) {
    	background-color: lightgrey;
	}

</style>

<body>
<br>
<?php
	
	require("header.php");
	
	session_start();
	if(isset($_SESSION["user_name"]) && $_SESSION["user_name"]!=""){
		echo "Hello ".$_SESSION["user_name"];
        } else {
			header ("Location: login_form.php");	
		}

	$where="";
	if(isset($_REQUEST['pname']) && $_REQUEST['pname']!=""){		
			$where = $where . " AND `pc_name` = '$_REQUEST[pname]'";	
	}
	
	if(isset($_REQUEST['uname']) && $_REQUEST['uname']!=""){

			$where = $where . " AND `user_name` = '$_REQUEST[uname]'";	
	
	}

	$totalrecords_sql = "SELECT count(*) AS count FROM `system` WHERE 1=1 $where";
	$totalrecords_res = $conct->query($totalrecords_sql);
//	error_log("\n===var = $totalrecords ===\n");
//	echo $totalrecords_sql;

	$total = $totalrecords_res->fetch(PDO::FETCH_ASSOC);
	$totalrecords = $total["count"];
//	error_log("\n===" . $totalrecords . "===\n");
//	$totalpage = ceil($totalrecords/$limit);
	
	$offset_limit="";
	$limit="10";

	if(isset($_REQUEST['offset']) && isset($_REQUEST['limit']) && $_REQUEST['offset']!="" && $_REQUEST['limit']!=""){
	
		$offset_limit = " LIMIT $_REQUEST[offset],$_REQUEST[limit]";
		$limit = $_REQUEST['limit'];
	}


	$select_sql = "SELECT * FROM `system` WHERE 1=1 $where ORDER BY `id` ASC $offset_limit";
	
//	echo $select_sql;

	$res = $conct->query($select_sql);
//	echo "<pre>";
//		var_dump($res);


?>
<br>
<div> <a>
<div style="align:center; white-space: nowrap;">
	<div align="left">
		<b>Dashboard</b>
	</div>
	<div>
	    <a style="display:inline-block; float:left" href="logout.php">Logout</a>
	</div>
<br>
	<div>
		<a href='add_form.php'>Add</a>
<div>
<br>

<!-- search box for filter -->
<form class="search" action="dashboard.php" style="margin:auto;max-width:300px">
  <input type="text" placeholder="Search by pc_name..." name="pname"><br>
<br>
  <input type="text" placeholder="Search by username..." name="uname"><br>
<br>
  <input type="submit" value="search">
</form>
<br><br>

</div align="center">
	<form action="importcsv.php" method="post" enctype="multipart/form-data">
		
		<input type="file" name="upload"/>
		<input type="submit" value="Save"/>
		
	</form>
</div>

<!-- getting data from database and dispalying in form of table -->
<br>

<div align="center">
<a href="exportcsv.php">Export Csv</a>
</div>

<br>
<div align="center">
<table border=1px>	
	<tr>
		<th>Sr.No.</th>
		<th>PC_Name</th>
		<th>Purchase_date</th>
		<th>User_name</th>
		<th>Action</td>
	</tr>
<?php

	while($row = $res->fetch(PDO::FETCH_ASSOC)){
	
?>
		
	<tr>
		<td><?php echo $row["id"]; ?></td>
		<td><?php echo $row["pc_name"];?></td>
		<td><?php echo $row["purchase_date"];?></td>
		<td><?php echo $row["user_name"];?></td>
		<td><a href='edit_form.php?id=<?php echo $row["id"];?>&amp; name=<?php echo $row["pc_name"];?>&amp; user_name=<?php echo $row["user_name"];?>'>edit</a>
		<a href='delete_action.php?id=<?php echo $row["id"];?>' onclick="javascript: return confirm('Are you sure you want to delete this Entry?')">Delete</a></td>
	</tr>
<?php
	} 
?>
</table>
</div>
<br>
	<hr>
	
	<?php
		$pages_count =ceil($totalrecords/$limit);
		
		for($i=0; $i<$pages_count; $i++){
			$offset = $i*$limit;
	?>
		
		<a href="dashboard.php?offset=<?php echo $offset; ?>&limit=<?php echo $limit;?>"> <?php echo $i+1; ?> </a>&nbsp;
				
	<?php
		}

	?>

	
</body>
</html>

