<?php

function getConn(){
	
	$servername = "localhost";
	$username = "root";
	$password = "";

	try {
		$conn = new PDO("mysql:host=$servername;dbname=cidb", $username, $password);
		// set the PDO error mode to exception
	//	$conct = $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	//	echo "Connected successfully";
		}
	catch(PDOException $e)
		{
		echo "Connection failed: " . $e->getMessage();
		}
	return $conn;
}


function authorization(){
	
	if(!isset($_SESSION["user_name"])){
		header("Location: login_form.php");
	}
}

?>